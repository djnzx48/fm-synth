#!/usr/bin/env bash

mkdir -p cache

# install make

MAKE_VERSION=4.3
MAKE_URL="https://ftp.gnu.org/gnu/make/make-$MAKE_VERSION.tar.gz"

if [ ! -f cache/make.tgz ]; then
    curl -L "$MAKE_URL" > cache/make.tgz
    tar xvf cache/make.tgz -C cache
fi

cd cache/make-*

./configure --prefix=/usr

make -j install

cd ../..

# install sjasmplus

SJASMPLUS_URL='https://github.com/z00m128/sjasmplus/archive/v1.18.2.tar.gz'

if [ ! -f cache/sjasmplus.tar.gz ]; then
    curl -L "$SJASMPLUS_URL" > cache/sjasmplus.tar.gz
    tar -x -f cache/sjasmplus.tar.gz -C cache
fi

cd cache/sjasmplus-1.18.2

make -j install

cd ../..
