#!/usr/bin/env bash

DL_DIR='bin'
ZEUS_DIR='tools/zeus'
BAS2TAP_DIR='tools/bas2tap'
GENTAPE_DIR='tools/GenTape'
ZX0_DIR='tools/zx0'

# ZEUS_URL='https://web.archive.org/web/20190930000245/http://www.desdes.com/products/oldfiles/zeus.zip'
BAS2TAP_URL='https://web.archive.org/web/20180828130244/http://www.worldofspectrum.org/pub/sinclair/tools/pc/bas2tap25-win.zip'
GENTAPE_URL='https://web.archive.org/web/20180114082131/http://retrolandia.net/foro/attachment.php?aid=345'
ZEUS_IDE_URL='https://web.archive.org/web/20200216112553/http://www.desdes.com/products/oldfiles/zeustest.exe'
ZCL_URL='https://web.archive.org/web/20200216112345/http://www.desdes.com/products/oldfiles/zcltest.exe'
ZX0_URL='https://github.com/einar-saukas/ZX0/raw/c27a1a31c42385d995682a34aba5accec2944158/win/zx0.exe'

if ! [ -d "$DL_DIR" ]; then
    mkdir -p "$DL_DIR"
fi


#### ZEUS ####

if ! [ -d "$ZEUS_DIR" ]; then
    mkdir -p "$ZEUS_DIR"
fi

echo Downloading latest build of Zeus...
echo

curl -L "$ZEUS_IDE_URL" > "$ZEUS_DIR"/zeus.exe
curl -L "$ZCL_URL" > "$ZEUS_DIR"/zcl.exe

echo
echo Done.
echo

#### BAS2TAP ####

if ! [ -d "$BAS2TAP_DIR" ]; then
    mkdir -p "$BAS2TAP_DIR"
fi

echo Downloading latest build of bas2tap...
echo

curl -L "$BAS2TAP_URL" > "$DL_DIR"/bas2tap.zip

echo
echo Unpacking bas2tap...
echo

echo
echo Done.
echo

# extract bas2tap
unzip -o "${DL_DIR}/bas2tap.zip" -d"$BAS2TAP_DIR"

#### GENTAPE ####

if ! [ -d "$GENTAPE_DIR" ]; then
    mkdir -p "$GENTAPE_DIR"
fi

echo Downloading latest build of GenTape...
echo

curl -L "$GENTAPE_URL" > "$DL_DIR"/GenTape.zip

echo
echo Unpacking GenTape...
echo

# extract GenTape
unzip -o "${DL_DIR}/GenTape.zip" -d"$GENTAPE_DIR"

#### ZX0 ####

if ! [ -d "$ZX0_DIR" ]; then
    mkdir -p "$ZX0_DIR"
fi

echo Downloading latest build of ZX0...
echo

curl -L "$ZX0_URL" > "$ZX0_DIR"/zx0.exe

echo
echo Done.
echo

echo
echo Setup complete!
