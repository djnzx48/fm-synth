#!/usr/bin/env bash

set -xev

mkdir fuse
cd fuse

LIBSPEC_VERSION=1.4.4
LIBSPEC_URL="https://sourceforge.net/projects/fuse-emulator/files/libspectrum/$LIBSPEC_VERSION/libspectrum-$LIBSPEC_VERSION.tar.gz/download"

echo
echo Downloading libspectrum...
echo

curl -L "$LIBSPEC_URL" -o libspectrum.tgz

tar xvf libspectrum.tgz

cd libspectrum-*

echo
echo Compiling libspectrum...
echo

./configure --with-fake-glib --prefix=/usr

make install

cd ..

VERSION=1.5.7
URL="https://sourceforge.net/projects/fuse-emulator/files/fuse/$VERSION/fuse-$VERSION.tar.gz/download"

echo
echo Downloading Fuse...
echo

curl -L "$URL" -o fuse.tgz

tar xf fuse.tgz

cd fuse-*

patch -p1 -i ../../fuse.patch

echo
echo Compiling Fuse...
echo

./configure --with-null-ui --with-fake-glib

make install
