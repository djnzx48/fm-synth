SONG_SPEED equ 11

; ============================================

SAMPLE_TABLE_ENTRY_SIZE equ 3

BEGIN_PAGE_TABLE("sample_table", false)

    KICK_SAMPLE_ID      equ LOW $

    dw kick_sample              ; address
    db kick_sample_length       ; assumed to fit in 8 bits

    KICK2_SAMPLE_ID     equ LOW $

    dw kick2_sample
    db kick2_sample_length

    SNARE_SAMPLE_ID     equ LOW $

    dw snare_sample
    db snare_sample_length

    SNARE_LIGHT_SAMPLE_ID   equ LOW $

    dw snare_light_sample
    db snare_light_sample_length

    HAT_SAMPLE_ID       equ LOW $

    dw hat_sample
    db hat_sample_length

    TOM_SAMPLE_ID       equ LOW $

    dw tom_sample
    db tom_sample_length

    CYMBAL_SAMPLE_ID    equ LOW $

    dw cymbal_sample
    db cymbal_sample_length

    SAMPLE_COUNT        equ ($ - sample_table) / SAMPLE_TABLE_ENTRY_SIZE

END_PAGE_TABLE("sample_table")

; ============================================

; stored as delay, carrier, mod tuples

; NOTE: the delay value in the first row of the instrument
; must have 1 added to it, as with the first delay value
; of a pattern. Otherwise the first row will be entirely
; skipped, and furthermore things will get messed up due
; to the way the instrument code works. So remember to
; always add 1 to this value.
;
; The exception is 'empty' instruments because we want to
; skip the first row deliberately for these instruments.

epiano_instrument:
    db 20+1, HIGH epiano_waveform_f, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_d, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_b, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_9, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_7, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_5, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_3, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_1, HIGH sawtooth_waveform_mod
epiano_instrument_loop:
    db 20, HIGH empty_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw epiano_instrument_loop

bass_instrument:
    db 1+1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
bass_instrument_loop:
    db 1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw bass_instrument_loop

theremin_instrument:
    db 1+1, HIGH theremin_waveform
theremin_instrument_loop:
    db 1, HIGH theremin_waveform
    db 0
    dw theremin_instrument_loop

; ============================================

BEGIN_PAGE_TABLE("song_table", false)

SONG_START              equ LOW $

    ;   a    b    c    d    e
    db p00, p00, p00, p02, p04

PATTERN_POS_LOOP_START  equ LOW $

    db p06, p08, p0a, p0c, p0e

END_PATTERN_POS         equ LOW $

END_PAGE_TABLE("song_table")

BEGIN_PAGE_TABLE("pattern_table", false)

p00 equ LOW $
    dw empty_pattern
p02 equ LOW $
    dw empty_drums_pattern
p04 equ LOW $
    dw event_pattern0
p06 equ LOW $
    dw pattern_a
p08 equ LOW $
    dw pattern_b
p0a equ LOW $
    dw pattern_c
p0c equ LOW $
    dw pattern_d
p0e equ LOW $
    dw pattern_e

END_PAGE_TABLE("pattern_table")

; ============================================

    ; NOTE: length of pattern is obtained from the sample pattern
    ; (channel d), as the byte immediately following the timer.

empty_pattern:
    db 1                ; timer

    ; important that the song begins with a Note_Off -
    ; it initialises the channel with the empty instrument
    db 64, Note_Off

empty_drums_pattern:
    db 64+1, 64         ; timer, length

event_pattern0:
    db 1                ; timer

    db 60, SET_SONG_SPEED
    db 1

    db 1, CHANNEL_B_INSTRUMENT
    dw epiano_instrument

    db 1, CHANNEL_B_ORNAMENT
    db MAJOR_7TH_ORNAMENT

    db 1, CHANNEL_B_PITCH_BEND
    dw 1
    db 0

    db 1, SET_SONG_SPEED
    db 11

pattern_a:
    db 1                ; timer

    db 64, Note_Off

pattern_b:
    db 1                ; timer

    db 16, A_3
    db 16, A_3
    db 16, A_3
    db 16, A_3

pattern_c:
    db 1                ; timer

    db 64, Note_Off

pattern_d:
    db 64+1, 64         ; timer, length

pattern_e:
    db 64+1             ; timer

; ============================================

END_BANK(2)

; ============================================

align 256
sawtooth_waveform_mod:
    ds 256

; ============================================

bass_waveform:
    ds 256
theremin_waveform:
    ds 256
epiano_waveform_1:
    ds 256
epiano_waveform_3:
    ds 256
epiano_waveform_5:
    ds 256
epiano_waveform_7:
    ds 256
epiano_waveform_9:
    ds 256
epiano_waveform_b:
    ds 256
epiano_waveform_d:
    ds 256
epiano_waveform_f:
    ds 256

; ============================================

BEGIN_BANK(0)

; ============================================

BEGIN_SAMPLE("kick_sample")

    import_bin "../bin/sample/wnd_kick.raw"

END_SAMPLE("kick_sample")

; ============================================

BEGIN_SAMPLE("kick2_sample")

    import_bin "../bin/sample/wnd_kick2.raw"

END_SAMPLE("kick2_sample")

; ============================================

BEGIN_SAMPLE("snare_sample")

    import_bin "../bin/sample/wnd_snare.raw"

END_SAMPLE("snare_sample")

; ============================================

BEGIN_SAMPLE("snare_light_sample")

    import_bin "../bin/sample/wnd_snare_light.raw"

END_SAMPLE("snare_light_sample")

; ============================================

BEGIN_SAMPLE("hat_sample")

    import_bin "../bin/sample/wnd_hat.raw"

END_SAMPLE("hat_sample")

; ============================================

BEGIN_SAMPLE("tom_sample")

    import_bin "../bin/sample/wnd_tom.raw"

END_SAMPLE("tom_sample")

; ============================================

BEGIN_SAMPLE("cymbal_sample")

    import_bin "../bin/sample/wnd_cymbal.raw"

END_SAMPLE("cymbal_sample")

; ============================================

END_BANK(0)
