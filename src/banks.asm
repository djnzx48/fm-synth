bank_start_0 = $c000
bank_start_1 = $c000
bank_start_2 = $8000
bank_start_3 = $c000
bank_start_4 = $c000
bank_start_5 = $4000
bank_start_6 = $c000
bank_start_7 = $c000

bank_size_0 = 0
bank_size_1 = 0
bank_size_2 = 0
bank_size_3 = 0
bank_size_4 = 0
bank_size_5 = $2000 ; ignore initial portion
bank_size_6 = 0
bank_size_7 = 0

current_bank = -1
mod_base = 0

ASSERT_BANK_START   macro()
    zeusassert ($ & $3fff) = 0

mend

BEGIN_BANK_PC   macro(bank, start_addr)
    if ::current_bank <> -1
        zeusprint "Calling END_BANK on previous bank"
        END_BANK(::current_bank)
    endif

    ::current_bank = bank

    if bank = 0
        ::mod_base = start_addr - ::bank_start_0
        org start_addr + ::bank_size_0
    elseif bank = 1
        ::mod_base = start_addr - ::bank_start_1
        org start_addr + ::bank_size_1
    elseif bank = 2
        ::mod_base = start_addr - ::bank_start_2
        org start_addr + ::bank_size_2
    elseif bank = 3
        ::mod_base = start_addr - ::bank_start_3
        org start_addr + ::bank_size_3
    elseif bank = 4
        ::mod_base = start_addr - ::bank_start_4
        org start_addr + ::bank_size_4
    elseif bank = 5
        ::mod_base = start_addr - ::bank_start_5
        org start_addr + ::bank_size_5
    elseif bank = 6
        ::mod_base = start_addr - ::bank_start_6
        org start_addr + ::bank_size_6
    elseif bank = 7
        ::mod_base = start_addr - ::bank_start_7
        org start_addr + ::bank_size_7
    else
        zeuserror "Invalid bank! ", bank
    endif

    disp zeuspage(bank) - start_addr

mend

BEGIN_BANK  macro(bank)
    if bank = 0
        BEGIN_BANK_PC(bank, ::bank_start_0)
    elseif bank = 1
        BEGIN_BANK_PC(bank, ::bank_start_1)
    elseif bank = 2
        BEGIN_BANK_PC(bank, ::bank_start_2)
    elseif bank = 3
        BEGIN_BANK_PC(bank, ::bank_start_3)
    elseif bank = 4
        BEGIN_BANK_PC(bank, ::bank_start_4)
    elseif bank = 5
        BEGIN_BANK_PC(bank, ::bank_start_5)
    elseif bank = 6
        BEGIN_BANK_PC(bank, ::bank_start_6)
    elseif bank = 7
        BEGIN_BANK_PC(bank, ::bank_start_7)
    else
        zeuserror "Invalid bank! ", bank
    endif
mend

END_BANK    macro(bank)
    if ::current_bank = -1
        zeuserror "END_BANK used too many times"
    endif

    if bank <> current_bank
        zeuserror "Non-matching END_BANK"
    endif

    if ::current_bank = 0
        ::bank_size_0 = $ - ::bank_start_0 - ::mod_base
    elseif ::current_bank = 1
        ::bank_size_1 = $ - ::bank_start_1 - ::mod_base
    elseif ::current_bank = 2
        ::bank_size_2 = $ - ::bank_start_2 - ::mod_base
    elseif ::current_bank = 3
        ::bank_size_3 = $ - ::bank_start_3 - ::mod_base
    elseif ::current_bank = 4
        ::bank_size_4 = $ - ::bank_start_4 - ::mod_base
    elseif ::current_bank = 5
        ::bank_size_5 = $ - ::bank_start_5 - ::mod_base
    elseif ::current_bank = 6
        ::bank_size_6 = $ - ::bank_start_6 - ::mod_base
    else
        ::bank_size_7 = $ - ::bank_start_7 - ::mod_base
    endif

    disp 0

    ::current_bank = -1
mend

; these have to be global or it won't work
bank_0_type = "        WAVES 0"
bank_1_type = "      SAMPLES 1"
bank_2_type = "    CODE/SONG 2"
bank_3_type = "      SAMPLES 3"
bank_4_type = "        WAVES 4"
bank_5_type = "  SCR/SAMPLES 5"
bank_6_type = "        WAVES 6"
bank_7_type = "      SAMPLES 7"

total_type  = "        TOTAL *"

SHOW_BANK_USAGE macro()
    ; account for initial portion
    if (::bank_size_5 < $2000)
        size_5 = $2000
    else
        size_5 = ::bank_size_5
    endif

    zeusprint ""

    DRAW_BAR(::bank_0_type, ::bank_size_0, 16384)
    DRAW_BAR(::bank_1_type, ::bank_size_1, 16384)
    DRAW_BAR(::bank_2_type, ::bank_size_2, 16384)
    DRAW_BAR(::bank_3_type, ::bank_size_3, 16384)
    DRAW_BAR(::bank_4_type, ::bank_size_4, 16384)
    DRAW_BAR(::bank_5_type,  (size_5),     16384)
    DRAW_BAR(::bank_6_type, ::bank_size_6, 16384)
    DRAW_BAR(::bank_7_type, ::bank_size_7, 16384)

    zeusprint ""

    total = ::bank_size_0
    total = total + ::bank_size_1
    total = total + ::bank_size_2
    total = total + ::bank_size_3
    total = total + ::bank_size_4
    total = total + size_5
    total = total + ::bank_size_6
    total = total + ::bank_size_7

    DRAW_BAR(::total_type, (total), 8 * 16384)

    zeusprint ""

mend

DRAW_BAR    macro(bank_label, bank_size, total_bank_size)
    total_bar_width = 32

    if (bank_size > total_bank_size)
        zeuserror "Bank ", bank_label, " overflowed at ", bank_size, " bytes."
    endif

    print_string = " ["

    for bar_count = 0 to total_bar_width-1
        size_so_far = bar_count*total_bank_size/total_bar_width

        if bank_size > size_so_far
            print_string = print_string + "#"
        else
            print_string = print_string + "."
        endif
    next

    print_string = print_string + "] "

    zeusprint bank_label, print_string, bank_size
mend
