    DEVICE ZXSPECTRUM128

ROM_load    equ $0562               ; LD-BYTES, but some setup code is skipped.
NXTLIN      equ $5c55               ; address of next line in program.

DZX0_SIZE   equ dzx0_standard_end-dzx0_standard
DZX0_BASE   equ $6000-DZX0_SIZE     ; location of ZX0 decompressor.
RAMTOP      equ DZX0_BASE-1

    include "../bin/entry.asm"
    include "../bin/zx0_info.asm"

decompressed_data equ $6000
decompressed_data_end equ decompressed_data+decompressed_size

compressed_data_end equ decompressed_data_end+zx0delta
compressed_data equ compressed_data_end-compressed_size

program:
    dw $0000                ; Line number (big-endian)
    dw line_0_end-line_0    ; Line length
line_0:
    db $da                  ; PAPER
    db $c3                  ; NOT
    db $a7                  ; PI
    db ':'                  ; :
    db $da                  ; INK
    db $a7                  ; PI
    db '+'                  ; +
    db $a7                  ; PI
    db ':'                  ; :
    db $e7                  ; BORDER
    db $c3                  ; NOT
    db $a7                  ; PI
    db ':'                  ; :
    db $fd                  ; CLEAR
    db '0'                  ; 0
    db $0e                  ; <number>
        db 0
        db 0
        dw RAMTOP           ; RAMTOP
        db 0
    db ':'                  ; :
    db $f9                  ; RANDOMIZE
    db $c0                  ; USR
    db $b0                  ; VAL
    db '"'                  ; "
    db $be                  ; PEEK
    db '23637'              ; 23637
    db '+'                  ; +
    db '2'                  ; 2
    db '5'                  ; 5
    db '6'                  ; 6
    db '*'                  ; *
    db $be                  ; PEEK
    db '23638'              ; 23638
    db '"'                  ; "
    ; db ':'                  ; :
    db $0d ; ???
line_0_end:

boot:
    di

    ; copy ZX0 decompressor to desired location

    ; calculate decompressor start address in HL
    ld hl, boot_end-boot
    add hl, bc

    ; paging port
    ld bc, $7ffd
    ld a, $50

    ; page in required RAM bank
    out (c), a

    ; put destination address in DE
    ld de, DZX0_BASE

    ; decompressor size in BC
    ld bc, dzx0_standard_end-dzx0_standard

    ; copy decompressor to destination address
    ldir

    ; address and length of data to load
    ld ix, compressed_data
    ld de, compressed_size

    ; load data block and set carry for loading (not verify)
    scf
    sbc a, a
    ex af, af'

    call ROM_load

    ; decompress loaded block
    ld hl, compressed_data
    ld de, $6000
    call DZX0_BASE

    ; start the program
    jp entry

boot_end:
    DISP DZX0_BASE
    include "dzx0_standard.asm"
dzx0_standard_end:
    ENT
program_end:

    EMPTYTAP "../bin/loader.tap"
    SAVETAP "../bin/loader.tap", BASIC, "Synth", program, program_end-program, 0
