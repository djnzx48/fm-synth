zeusemulate "128K"

CONTENDED_BANK_0    equ 1
CONTENDED_BANK_1    equ 3
NONCONTENDED_BANK_0 equ 4
NONCONTENDED_BANK_1 equ 6

include "synth.asm"

output_bin "../bin/bank0.bin", bank_start_0, bank_size_0
output_bin "../bin/bank2.bin", bank_start_2, bank_size_2
output_bin "../bin/bank5.bin", bank_start_5 + $2000, bank_size_5 - $2000

output_list "../bin/synth.list"

output_text "../bin/entry.asm", "entry equ ", TOSTR(entry)
