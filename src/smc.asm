SMC_LD  macro(smc_reg, smc_label)
    code_prefix = -1

    ; check register
    if (smc_reg = a)

        reg_size    = 1
        code_byte   = Z_LD_A_N

    elseif (smc_reg = b)

        reg_size    = 1
        code_byte   = Z_LD_B_N

    elseif (smc_reg = c)

        reg_size    = 1
        code_byte   = Z_LD_C_N

    elseif (smc_reg = d)

        reg_size    = 1
        code_byte   = Z_LD_D_N

    elseif (smc_reg = e)

        reg_size    = 1
        code_byte   = Z_LD_E_N

    elseif (smc_reg = h)

        reg_size    = 1
        code_byte = Z_LD_H_N

    elseif (smc_reg = l)

        reg_size    = 1
        code_byte = Z_LD_L_N

    elseif (smc_reg = ixh)

        reg_size    = 1
        code_byte   = Z_LD_H_N
        code_prefix = Z_PREFIX_IX

    elseif (smc_reg = ixl)

        reg_size    = 1
        code_byte   = Z_LD_L_N
        code_prefix = Z_PREFIX_IX

    elseif (smc_reg = iyh)

        reg_size    = 1
        code_byte   = Z_LD_H_N
        code_prefix = Z_PREFIX_IY

    elseif (smc_reg = iyl)

        reg_size    = 1
        code_byte   = Z_LD_L_N
        code_prefix = Z_PREFIX_IY

    elseif (smc_reg = bc)

        reg_size    = 2
        code_byte   = Z_LD_BC_NN

    elseif (smc_reg = de)

        reg_size    = 2
        code_byte   = Z_LD_DE_NN

    elseif (smc_reg = hl)

        reg_size    = 2
        code_byte   = Z_LD_HL_NN

    elseif (smc_reg = sp)

        reg_size    = 2
        code_byte   = Z_LD_SP_NN

    elseif (smc_reg = ix)

        reg_size    = 2
        code_byte   = Z_LD_HL_NN
        code_prefix = Z_PREFIX_IX

    elseif (smc_reg = iy)

        reg_size    = 2
        code_byte   = Z_LD_HL_NN
        code_prefix = Z_PREFIX_IY

    else

        zeuserror "Invalid SMC register! (", smc_reg, ")"

    endif

    SMC_OP((smc_label), (reg_size), (code_byte), (code_prefix))

mend

; ============================================

SMC_LD_NN macro(smc_reg, smc_label)

    ; MAYBEDO: make this work for IX and IY
    code_prefix = -1

    ; check register
    if (smc_reg = a)

        code_byte   = Z_LD_NN_A

    elseif (smc_reg = bc)

        code_byte   = Z_LD_NN_BC
        code_prefix = Z_PREFIX_EX

    elseif (smc_reg = de)

        code_byte   = Z_LD_NN_DE
        code_prefix = Z_PREFIX_EX

    elseif (smc_reg = hl)

        code_byte   = Z_LD_NN_HL
        ; no prefix

    elseif (smc_reg = sp)

        code_byte   = Z_LD_NN_SP
        code_prefix = Z_PREFIX_EX

    else

        zeuserror "Invalid SMC register! (", smc_reg, ")"

    endif

    SMC_OP((smc_label), 2, (code_byte), (code_prefix))

mend

; ============================================

SMC_LD_R_NN macro(smc_reg, smc_label)

    ; MAYBEDO: make this work for IX and IY
    code_prefix = -1

    ; check register
    if (smc_reg = a)

        code_byte   = Z_LD_R_A_NN

    elseif (smc_reg = bc)

        code_byte   = Z_LD_R_BC_NN
        code_prefix = Z_PREFIX_EX

    elseif (smc_reg = de)

        code_byte   = Z_LD_R_DE_NN
        code_prefix = Z_PREFIX_EX

    elseif (smc_reg = hl)

        code_byte   = Z_LD_R_HL_NN
        ; no prefix

    elseif (smc_reg = sp)

        code_byte   = Z_LD_R_SP_NN
        code_prefix = Z_PREFIX_EX

    else

        zeuserror "Invalid SMC register! (", smc_reg, ")"

    endif

    SMC_OP((smc_label), 2, (code_byte), (code_prefix))

mend

; ============================================

SMC_ADD_A macro(smc_label)

    SMC_OP ((smc_label), 1, Z_ADD_A_N, -1)

mend

; ============================================

SMC_CP  macro(smc_label)

    SMC_OP ((smc_label), 1, Z_CP_N, -1)

mend

; ============================================

SMC_JP  macro(smc_label)

    SMC_OP ((smc_label), 2, Z_JP_NN, -1)

mend

; ============================================

SMC_JR  macro(smc_label)

    SMC_OP ((smc_label), 1, Z_JR_N, -1)

mend

; ============================================

SMC_OP  macro(smc_label, reg_size, code_byte, code_prefix)

    noflow

    ; plant the prefix byte
    if (code_prefix != -1)
        db code_prefix

    endif

    ; plant the code byte
    db code_byte

    ; place the label
    ::LABEL(smc_label)

    ; plant the operand
    if (reg_size = 1)

        db 0

    elseif (reg_size = 2)

        dw 0

    else
        zeuserror "Invalid SMC register size!"

    endif

mend

