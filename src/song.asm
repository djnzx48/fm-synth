SONG_SPEED equ 11

; ============================================

SAMPLE_TABLE_ENTRY_SIZE equ 3

BEGIN_PAGE_TABLE("sample_table", false)

    KICK_SAMPLE_ID      equ LOW $

    dw kick_sample              ; address
    db kick_sample_length       ; assumed to fit in 8 bits

    KICK2_SAMPLE_ID     equ LOW $

    dw kick2_sample
    db kick2_sample_length

    SNARE_SAMPLE_ID     equ LOW $

    dw snare_sample
    db snare_sample_length

    SNARE_LIGHT_SAMPLE_ID   equ LOW $

    dw snare_light_sample
    db snare_light_sample_length

    HAT_SAMPLE_ID       equ LOW $

    dw hat_sample
    db hat_sample_length

    TOM_SAMPLE_ID       equ LOW $

    dw tom_sample
    db tom_sample_length

    CYMBAL_SAMPLE_ID    equ LOW $

    dw cymbal_sample
    db cymbal_sample_length

    SAMPLE_COUNT        equ ($ - sample_table) / SAMPLE_TABLE_ENTRY_SIZE

END_PAGE_TABLE("sample_table")

; ============================================

; stored as delay, carrier, mod tuples

; NOTE: the delay value in the first row of the instrument
; must have 1 added to it, as with the first delay value
; of a pattern. Otherwise the first row will be entirely
; skipped, and furthermore things will get messed up due
; to the way the instrument code works. So remember to
; always add 1 to this value.
;
; The exception is 'empty' instruments because we want to
; skip the first row deliberately for these instruments.

epiano_instrument:
    db 20+1, HIGH epiano_waveform_f, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_d, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_b, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_9, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_7, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_5, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_3, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_1, HIGH sawtooth_waveform_mod
epiano_instrument_loop:
    db 20, HIGH empty_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw epiano_instrument_loop

bass_instrument:
    db 1+1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
bass_instrument_loop:
    db 1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw bass_instrument_loop

theremin_instrument:
    db 1+1, HIGH theremin_waveform
theremin_instrument_loop:
    db 1, HIGH theremin_waveform
    db 0
    dw theremin_instrument_loop

; ============================================

    ; NOTE: length of pattern is obtained from the sample pattern
    ; (channel d), as the byte immediately following the timer.

nothing_pattern64:
    db 1+64             ; timer

empty_pattern:
    db 1                ; timer

    ; important that the song begins with a Note_Off -
    ; it initialises the channel with the empty instrument
    db 64, Note_Off

empty_drums_pattern:
    db 64+1, 64         ; timer, length

empty_drums_pattern_short:
    db 48+1, 48         ; timer, length

drums_pattern_intro:
    db 1, 32

    db 4*8, SNARE_SAMPLE_ID
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

drums_intro_end:
    db 1, 48            ; timer, length

    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 2, SNARE_LIGHT_SAMPLE_ID
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, TOM_SAMPLE_ID
    ;
    ;
    ;

drums_pattern:
    db 1, 64            ; timer, length

    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;

; ============================================

drums_pattern_crash:
    db 1, 64

    db 8, CYMBAL_SAMPLE_ID
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;

; ============================================

BEGIN_PAGE_TABLE("song_table", false)

SONG_START              equ LOW $

    ;   a    b    c    d    e
    db p00, p00, p00, p82, p84
    db p14, p3e, p3c, p10, p7e

    ; intro
    db p16, p40, p48, p3a, p80
    db p18, p4a, p42, p02, p78
    db p1a, p44, p4c, p3a, p7a
    db p1c, p4e, p46, p12, p7c

    ; verse
    db p04, p08, p0c, p3a, p78

PATTERN_POS_LOOP_START  equ LOW $

    db p06, p0a, p0e, p02, p78
    db p20, p52, p50, p02, p78
    db p22, p56, p54, p02, p78
    db p24, p5a, p58, p3a, p78
    db p26, p5e, p5c, p02, p78
    db p28, p62, p60, p02, p78
    db p2a, p66, p64, p36, p78

    ; chorus
    db p2c, p6a, p68, p3a, p78
    db p2e, p6a, p68, p02, p78
    db p30, p6e, p6c, p3a, p78
    db p32, p72, p70, p38, p78

    ; verse
    db p04, p76, p74, p3a, p78

END_PATTERN_POS         equ LOW $

END_PAGE_TABLE("song_table")

; ============================================

drums_goto_chorus:
    db 1, 64

    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, TOM_SAMPLE_ID
    ;
    ;
    ;

drums_exit_chorus:
    db 1, 48

    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, HAT_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, KICK2_SAMPLE_ID
    ;
    ;
    ;
    db 2, KICK_SAMPLE_ID
    ;
    db 2, SNARE_LIGHT_SAMPLE_ID
    ;
    db 2, SNARE_LIGHT_SAMPLE_ID
    ;
    db 2, SNARE_LIGHT_SAMPLE_ID
    ;
    db 4, SNARE_SAMPLE_ID
    ;
    ;
    ;
    db 4, TOM_SAMPLE_ID
    ;
    ;
    ;

bass_intro:
    db 1

    db 4, Cs3
    ;
    ;
    ;
    db 4*7, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

bass_intro0:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, E_5
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, E_5
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 8, B_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, Gs2
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, D_3
    ;
    ;
    ;
    db 4, Fs3
    ;
    ;
    ;

bass_intro1:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Cs5
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 2, Gs2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 8, B_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 2, G_2
    ;
    db 2, Note_Off
    ;
    db 2, Gs2
    ;
    db 2, Note_Off
    ;
    db 2, B_2
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 4, Cs3
    ;
    ;
    ;

bass_intro2:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 1, C_3
    db 3, Cs3
    ;
    ;
    db 4, A_2
    ;
    ;
    ;
    db 8, D_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, G_2
    ;
    db 2, Note_Off
    ;
    db 4, G_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, D_3
    ;
    ;
    ;

bass_intro3:
    db 1

    db 14, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    db 2, Note_Off
    ;
    db 2, Cs3
    ;
    db 2, Note_Off
    ;
    db 4, Cs3
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Cs3
    ;
    ;
    ;
    db 8, A_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;

bass_pattern0:
    db 1                ; timer

    db 4, D_3
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 3, A_4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 3, Fs4
    ;
    ;
    db 1, Note_Off
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, G_2
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, Cs5
    ;
    ;
    ;
    db 2, Cs3
    ;
    db 2, Note_Off
    ;

bass_pattern1:
    db 1                ; timer

    db 4, D_3
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 3, A_5
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, G_2
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, D_3
    ;
    db 2, Note_Off
    ;
    db 2, D_3
    ;
    db 2, Note_Off
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;

bass_pattern2:
    db 1

    db 4, D_3
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;

bass_pattern3:
    db 1

    db 4, D_3
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 1, B_4
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, C_3
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;

bass_pattern4:
    db 1

    db 4, D_3
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, Cs5
    ;
    ;
    ;
    db 4, Cs3
    ;
    ;
    ;

bass_pattern5:
    db 1

    db 4, D_3
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, D_5
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 12, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 2, D_3
    ;
    db 2, Note_Off
    ;
    db 4, D_3
    ;
    ;
    ;

; ============================================

BEGIN_PAGE_TABLE("pattern_table", false)

p00 equ LOW $
    dw empty_pattern
p02 equ LOW $
    dw drums_pattern
p04 equ LOW $
    dw bass_pattern0
p06 equ LOW $
    dw bass_pattern1
p08 equ LOW $
    dw keys_pattern_a0
p0a equ LOW $
    dw keys_pattern_a1
p0c equ LOW $
    dw keys_pattern_b0
p0e equ LOW $
    dw keys_pattern_b1
p10 equ LOW $
    dw drums_pattern_intro
p12 equ LOW $
    dw drums_intro_end
p14 equ LOW $
    dw bass_intro
p16 equ LOW $
    dw bass_intro0
p18 equ LOW $
    dw bass_intro1
p1a equ LOW $
    dw bass_intro2
p1c equ LOW $
    dw bass_intro3
p1e equ LOW $
    dw empty_drums_pattern
p20 equ LOW $
    dw bass_pattern2
p22 equ LOW $
    dw bass_pattern3
p24 equ LOW $
    dw bass_pattern4
p26 equ LOW $
    dw bass_pattern5
p28 equ LOW $
    dw bass_pattern6
p2a equ LOW $
    dw bass_pattern7
p2c equ LOW $
    dw bass_pattern8
p2e equ LOW $
    dw bass_pattern9
p30 equ LOW $
    dw bass_patternA
p32 equ LOW $
    dw bass_patternB
p34 equ LOW $
    dw empty_drums_pattern_short
p36 equ LOW $
    dw drums_goto_chorus
p38 equ LOW $
    dw drums_exit_chorus
p3a equ LOW $
    dw drums_pattern_crash
p3c equ LOW $
    dw keys_pattern_intro
p3e equ LOW $
    dw solo_intro
p40 equ LOW $
    dw solo_intro1
p42 equ LOW $
    dw solo_intro2
p44 equ LOW $
    dw solo_intro3
p46 equ LOW $
    dw solo_intro4
p48 equ LOW $
    dw keys_intro
p4a equ LOW $
    dw keys_intro1
p4c equ LOW $
    dw keys_intro2
p4e equ LOW $
    dw keys_intro3
p50 equ LOW $
    dw keys_pattern1
p52 equ LOW $
    dw keys_pattern2
p54 equ LOW $
    dw keys_pattern3
p56 equ LOW $
    dw keys_pattern4
p58 equ LOW $
    dw keys_pattern5
p5a equ LOW $
    dw keys_pattern6
p5c equ LOW $
    dw keys_pattern7
p5e equ LOW $
    dw keys_pattern8
p60 equ LOW $
    dw keys_pattern9
p62 equ LOW $
    dw keys_patternA
p64 equ LOW $
    dw keys_patternB
p66 equ LOW $
    dw keys_patternC
p68 equ LOW $
    dw keys_patternD
p6a equ LOW $
    dw keys_patternE
p6c equ LOW $
    dw keys_patternF
p6e equ LOW $
    dw keys_patternG
p70 equ LOW $
    dw keys_patternH
p72 equ LOW $
    dw keys_patternI
p74 equ LOW $
    dw keys_patternJ
p76 equ LOW $
    dw keys_patternK
p78 equ LOW $
    dw nothing_pattern64
p7a equ LOW $
    dw activate_osc_sync
p7c equ LOW $
    dw deactivate_osc_sync
p7e equ LOW $
    dw solo_intro_e
p80 equ LOW $
    dw solo_intro_1_e
p82 equ LOW $
    dw init_pattern_drums
p84 equ LOW $
    dw init_pattern_event

END_PAGE_TABLE("pattern_table")

; ============================================

bass_pattern6:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, A_5
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 2, Fs2
    ;
    db 2, Note_Off
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 8, G_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, As2
    ;
    ;
    ;

bass_pattern7:
    db 1

    db 8, B_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 8, Cs3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Cs3
    ;
    ;
    ;
    db 8, G_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 8, Cs3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, Cs3
    ;
    ;
    ;

bass_pattern8:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, E_5
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 2, Gs2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, E_5
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;
    db 2, Gs2
    ;
    db 2, Note_Off
    ;
    db 2, B_2
    ;
    db 2, Note_Off
    ;
    db 2, D_3
    ;
    db 2, Note_Off
    ;
    db 2, Cs3
    ;
    db 2, Note_Off
    ;

bass_pattern9:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, E_5
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 4, Gs2
    ;
    ;
    ;
    db 4, A_2
    ;
    ;
    ;
    db 4, E_5
    ;
    ;
    ;
    db 4, Fs2
    ;
    ;
    ;
    db 8, B_2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, Gs2
    ;
    ;
    ;
    db 4, B_2
    ;
    ;
    ;
    db 4, D_3
    ;
    ;
    ;
    db 4, Fs3
    ;
    ;
    ;

bass_patternA:
    db 1

    db 8, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 2, Cs3
    ;
    db 2, Note_Off
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 8, D_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 4, D_3
    ;
    ;
    ;
    db 6, G_2
    ;
    ;
    ;
    ; ----
    ;
    db 2, Note_Off
    ;
    db 4, D_3
    ;
    ;
    ;
    db 4, G_2
    ;
    ;
    ;

bass_patternB:
    db 1

    db 12, Fs2
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 8, Cs3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_2
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;
    db 2, C_3
    ;
    db 2, Note_Off
    ;
    db 4, A_2
    ;
    ;
    ;

keys_pattern_a0:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_4
    ;
    ;
    db 13, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, A_3
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 1, As3
    db 7, B_3
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, As3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternK:
    db 1

    db 3, D_4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 3, D_4
    ;
    ;
    db 13, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, A_3
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 1, As3
    db 7, B_3
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, As3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern_a1:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_5
    ;
    ;
    db 4*13 + 1, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern_b0:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, F_4
    db 2, Fs4
    ;
    db 13, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, G_4
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, G_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternJ:
    db 1

    db 3, Fs4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 1, F_4
    db 2, Fs4
    ;
    db 13, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, G_4
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, G_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern_b1:
    db 1

    db 8, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, F_5
    db 2, Fs5
    ;
    db 4*9 + 1, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern1:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, F_4
    db 2, Fs4
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 9, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, G_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern2:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_4
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 8, A_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 9, G_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, As3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern3:
    db 1

    db 2, E_3
    ;
    db 1, Fs3
    db 1, Note_Off
    db 1, A_3
    db 3, Note_Off
    ;
    ;
    db 2, D_5
    ;
    db 2, Note_Off
    ;
    db 12, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 7, C_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 1, Cs4
    db 12, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, F_4
    db 3, Fs4
    ;
    ;

keys_pattern4:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Fs4
    ;
    db 2, Note_Off
    ;
    db 12, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 7, A_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 1, As3
    db 14, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    db 2, Note_Off
    ;

keys_pattern5:
    db 1

    db 4, E_4
    ;
    ;
    ;
    db 4, D_4
    ;
    ;
    ;
    db 4, B_3
    ;
    ;
    ;
    db 4, D_4
    ;
    ;
    ;
    db 2, F_3
    ;
    db 2, E_3
    ;
    db 5, D_3
    ;
    ;
    ;
    ; ----
    db 23, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, G_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern6:
    db 4*12 + 1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, As3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern7:
    db 1

    db 1, F_4
    db 3, Fs4
    ;
    ;
    db 2, A_4
    ;
    db 2, Note_Off
    ;
    db 1, F_5
    db 2, Fs5
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 5, D_4
    ;
    ;
    ;
    ; ----
    db 11, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, As3
    db 7, B_3
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, D_4
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_pattern8:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, A_5
    ;
    ;
    db 21, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, F_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, E_4
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;

keys_pattern9:
    db 1

    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, D_5
    ;
    ;
    db 9, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, F_4
    ;
    ;
    ;
    db 3, E_4
    ;
    ;
    db 1, Note_Off
    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, E_4
    ;
    ;
    db 9, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternA:
    db 1

    db 1, F_3
    db 7, Fs3
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, F_4
    db 2, Fs4
    ;
    db 4*11 + 1, Note_Off
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternB:
    db 1

    db 11, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 12, Gs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 12, B_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 13, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 3, Note_Off
    ;
    ;

keys_patternC:
    db 1

    db 11, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 12, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 12, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Note_Off
    ;
    ;
    ;
    db 13, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 3, Note_Off
    ;
    ;

keys_patternD:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, Cs5
    ;
    db 14, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, B_4
    ;
    ;
    ;
    db 15, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 9, Gs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternE:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_4
    ;
    db 14, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Gs4
    ;
    ;
    ;
    db 15, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 8, C_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, Cs4
    db 7, Note_Off
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_patternF:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Cs5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 11, E_5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 11, Cs5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 11, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;

keys_patternG:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 11, B_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 11, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;
    db 11, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;

keys_patternH:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, C_4
    db 7, Cs4
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 3, E_3
    ;
    ;
    db 1, Note_Off
    db 2, E_3
    ;
    db 2, Note_Off
    ;
    db 4, E_4
    ;
    ;
    ;
    db 4, F_4
    ;
    ;
    ;

keys_patternI:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, Gs3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, C_4
    ;
    ;
    ;
    db 4, Cs4
    ;
    ;
    ;

keys_pattern_intro:
    db 1

    db 2, Cs4
    ;
    db 4*15 + 2, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

solo_intro:
    db 1

    db 2, Cs5
    ;
    db 2, Note_Off
    ;
    db 1, C_4
    db 23, Cs4
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, B_3
    ;
    db 2, A_3
    ;

solo_intro1:
    db 1

    db 2, B_3
    ;
    db 2, Cs4
    ;
    db 32, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, C_5
    db 6, Cs5
    ;
    ;
    ; ----
    ;
    ;
    db 1, C_5
    db 1, B_4
    db 1, A_4
    db 2, Note_Off
    ;
    db 4, B_4
    ;
    ;
    ;
    db 4, C_5
    ;
    ;
    ;
    db 2, A_4
    ;
    db 2, Note_Off
    ;
    db 2, Fs4
    ;
    db 2, Note_Off
    ;

solo_intro2:
    db 1

    db 4, B_4
    ;
    ;
    ;
    db 2, A_4
    ;
    db 2, Note_Off
    ;
    db 2, Fs4
    ;
    db 2, Note_Off
    ;
    db 9, Fs4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    db 1, E_4
    db 2, Note_Off
    ;
    db 1, C_4
    db 3, B_3
    ;
    ;
    db 2, A_3
    ;
    db 2, Note_Off
    ;
    db 2, B_3
    ;
    db 3, C_4
    ;
    ; ----
    db 3, Note_Off
    ;
    ;
    db 4, Fs4
    ;
    ;
    ;
    db 1, C_4
    db 2, Cs4
    ;
    db 1, Note_Off
    db 8, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, A_3
    ;
    ;
    ;
    db 4, Fs3
    ;
    ;
    ;

solo_intro3:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, C_5
    db 7, Cs5
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, E_5
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, D_5
    db 3, Note_Off
    ;
    ;
    db 1, Cs5
    db 3, Note_Off
    ;
    ;
    db 5, B_4
    ;
    ;
    ;
    ; ----
    db 2, C_5
    ;
    db 1, B_4
    db 12, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, Gs4
    db 1, G_4
    db 2, Note_Off
    ;
    db 8, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

solo_intro4:
    db 1

    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, C_4
    db 7, Cs4
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 11, G_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 5, Note_Off
    ; ----
    ;
    ;
    ;

keys_intro:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_4
    ;
    db 14, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Gs4
    ;
    ;
    ;
    db 20, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, Cs4
    ;
    ;
    ;
    db 1, C_4
    db 3, Note_Off
    ;
    ;
    db 1, Fs4
    db 3, Note_Off
    ;
    ;
    db 1, Gs4
    db 3, Note_Off
    ;
    ;

keys_intro1:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 2, A_4
    ;
    db 14, Note_Off
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 4, E_4
    ;
    ;
    ;
    db 16, D_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 1, Gs4
    db 3, Note_Off
    ;
    ;
    db 1, Fs4
    db 3, Note_Off
    ;
    ;
    db 2, C_4
    ;
    db 2, Note_Off
    ;

keys_intro2:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, B_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, Note_Off
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, E_4
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, B_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;

keys_intro3:
    db 8+1

    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 8, A_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 16, Gs3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    db 10, E_3
    ;
    ;
    ;
    ; ----
    ;
    ;
    ;
    ; ----
    ;
    db 6, Note_Off
    ;
    ; ----
    ;
    ;
    ;

init_pattern_drums:
    db 4+1, 4           ; timer, length

init_pattern_event:
    db 1

    db 1, CHANNEL_A_INSTRUMENT
    dw bass_instrument

    db 1, CHANNEL_B_INSTRUMENT
    dw epiano_instrument

    db 1, CHANNEL_B_OSC_INSTRUMENT
    dw theremin_instrument

    db 1, CHANNEL_C_INSTRUMENT
    dw epiano_instrument

activate_osc_sync:
    db 7+1

    db 1, OSC_SYNC_PARAMS
    dw $2400, -$0010

    db 48, CHANNEL_B_OSC_SYNC

    db 8, OSC_SYNC_PARAMS
    dw $0a00, -$000c

deactivate_osc_sync:
    db 8+1

    db 40, CHANNEL_B_FM

solo_intro_e:
    db 3+1

    db 1, OSC_SYNC_PARAMS
    dw $3000, -$0016

    db 60, CHANNEL_B_OSC_SYNC

solo_intro_1_e:
    db 4+1

    db 60, CHANNEL_B_FM

; ============================================

END_BANK(2)

; ============================================

align 256
sawtooth_waveform_mod:
    ds 256

; ============================================

bass_waveform:
    ds 256
theremin_waveform:
    ds 256
epiano_waveform_1:
    ds 256
epiano_waveform_3:
    ds 256
epiano_waveform_5:
    ds 256
epiano_waveform_7:
    ds 256
epiano_waveform_9:
    ds 256
epiano_waveform_b:
    ds 256
epiano_waveform_d:
    ds 256
epiano_waveform_f:
    ds 256

; ============================================

BEGIN_BANK(0)

; ============================================

BEGIN_SAMPLE("kick_sample")

    import_bin "../bin/sample/wnd_kick.raw"

END_SAMPLE("kick_sample")

; ============================================

BEGIN_SAMPLE("kick2_sample")

    import_bin "../bin/sample/wnd_kick2.raw"

END_SAMPLE("kick2_sample")

; ============================================

BEGIN_SAMPLE("snare_sample")

    import_bin "../bin/sample/wnd_snare.raw"

END_SAMPLE("snare_sample")

; ============================================

BEGIN_SAMPLE("snare_light_sample")

    import_bin "../bin/sample/wnd_snare_light.raw"

END_SAMPLE("snare_light_sample")

; ============================================

BEGIN_SAMPLE("hat_sample")

    import_bin "../bin/sample/wnd_hat.raw"

END_SAMPLE("hat_sample")

; ============================================

BEGIN_SAMPLE("tom_sample")

    import_bin "../bin/sample/wnd_tom.raw"

END_SAMPLE("tom_sample")

; ============================================

BEGIN_SAMPLE("cymbal_sample")

    import_bin "../bin/sample/wnd_cymbal.raw"

END_SAMPLE("cymbal_sample")

; ============================================

END_BANK(0)
