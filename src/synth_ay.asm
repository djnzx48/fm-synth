zeusemulate "128K"

DZX0_SIZE   equ dzx0_standard_end-dzx0_standard
DZX0_BASE   equ $6000-DZX0_SIZE     ; location of ZX0 decompressor.

INIT_SIZE   equ dzx0_standard-init
INIT_BASE   equ DZX0_BASE-INIT_SIZE ; location of init routine.

    include "../bin/entry.asm"
    include "../bin/zx0_info.asm"

decompressed_data equ $6000
decompressed_data_end equ decompressed_data+decompressed_size

compressed_data_end equ decompressed_data_end+zx0delta
compressed_data equ compressed_data_end-compressed_size

    ; AY file header

    ; NOTE - words are big-endian
    ; and must be aligned at 2 byte offsets

org $4000
ay_header_start:

    db "ZXAY"               ; FileID            longword
    db "EMUL"               ; TypeID            longword
    db 0                    ; FileVersion       byte
    db 0                    ; PlayerVersion     byte
    defwbe 0                ; PSpecialPlayer    smallint
    defwbe Author - $       ; PAuthor           smallint
    defwbe Misc - $         ; PMisc             smallint
    db 1 -1                 ; NumOfSongs        byte
    db 1 -1                 ; FirstSong         byte
    defwbe SongStructure - $; PSongsStructure   smallint

SongStructure:
    defwbe SongName - $     ; PSongName         smallint
    defwbe SongData - $     ; PSongData         smallint

SongData:
    db 0                    ; AChan             byte
    db 1                    ; BChan             byte
    db 2                    ; CChan             byte
    db 3                    ; Noise             byte
    defwbe 0                ; SongLength        word
    defwbe 0                ; FadeLength        word
    db 0                    ; HiReg             byte
    db 0                    ; LoReg             byte
    defwbe Points - $       ; PPoints           smallint
    defwbe Addresses - $    ; PAddresses        smallint

Points:

    defwbe 0                ; Stack             word
    defwbe init             ; Init              word
    defwbe 0                ; Inter             word

Addresses:

    defwbe Init_Start       ; Address1          word
    defwbe Init_Size        ; Length1           word
    defwbe Init - $         ; Offset1           word

    defwbe Data_Start       ; Address2          word
    defwbe Data_Size        ; Length2           word
    defwbe Data - $         ; Offset2           word

    defwbe 0                ; ENDWORD           word

Author:

    db "djnzx", 0

Misc:

    db 0

SongName:

    db "Hey Nineteen", 0

ay_header_end:

    org INIT_BASE
init:
    ; decompress loaded block
    ld hl, compressed_data
    ld de, $6000
    call DZX0_BASE

    ; start the program
    jp entry

    zeusassert ($ = DZX0_BASE)

    include "dzx0_standard.asm"
dzx0_standard_end:
init_end:

ay_header_size = ay_header_end - ay_header_start
init_size = init_end - init

Init equ ay_header_end
Data equ Init + init_size

Init_Start equ init
Init_Size equ init_size

Data_Start equ compressed_data
Data_Size equ compressed_size

output_bin "../bin/ay/header.bin", ay_header_start, ay_header_size
output_bin "../bin/ay/init.bin", init, init_size

output_list "../bin/ay/synth_ay.list"
