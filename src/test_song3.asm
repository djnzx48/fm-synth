; buffers per division = (3546900 / 331 / bpm / divisions_per_beat)
;                      = (3546900 / 331 / 116 / 6)
;                      = 15.3961

SONG_SPEED equ 15

; ============================================

SAMPLE_TABLE_ENTRY_SIZE equ 3

BEGIN_PAGE_TABLE("sample_table", false)

    KICK_SAMPLE_ID      equ LOW $

    dw kick_sample              ; address
    db kick_sample_length       ; assumed to fit in 8 bits

    KICK2_SAMPLE_ID     equ LOW $

    dw kick2_sample
    db kick2_sample_length

    SNARE_SAMPLE_ID     equ LOW $

    dw snare_sample
    db snare_sample_length

    SNARE_LIGHT_SAMPLE_ID   equ LOW $

    dw snare_light_sample
    db snare_light_sample_length

    HAT_SAMPLE_ID       equ LOW $

    dw hat_sample
    db hat_sample_length

    TOM_SAMPLE_ID       equ LOW $

    dw tom_sample
    db tom_sample_length

    CYMBAL_SAMPLE_ID    equ LOW $

    dw cymbal_sample
    db cymbal_sample_length

    SAMPLE_COUNT        equ ($ - sample_table) / SAMPLE_TABLE_ENTRY_SIZE

END_PAGE_TABLE("sample_table")

; ============================================

epiano_instrument:
    db 20+1, HIGH epiano_waveform_f, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_d, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_b, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_9, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_7, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_5, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_3, HIGH sawtooth_waveform_mod
    db 20, HIGH epiano_waveform_1, HIGH sawtooth_waveform_mod
epiano_instrument_loop:
    db 20, HIGH empty_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw epiano_instrument_loop

bass_instrument:
    db 1+1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
bass_instrument_loop:
    db 1, HIGH bass_waveform, HIGH sawtooth_waveform_mod
    db 0
    dw bass_instrument_loop

bass_instrument2:
    db 1+1, HIGH bass_waveform
bass_instrument2_loop:
    db 1, HIGH bass_waveform
    db 0
    dw bass_instrument2_loop

theremin_instrument:
    db 1+1, HIGH theremin_waveform
theremin_instrument_loop:
    db 1, HIGH theremin_waveform
    db 0
    dw theremin_instrument_loop

; ============================================

BEGIN_PAGE_TABLE("song_table", false)

SONG_START              equ LOW $

    ;   a    b    c    d    e
    db p00, p00, p00, p0c, p0e

PATTERN_POS_LOOP_START  equ LOW $

    db p04, p08, p0a, p02, p06

END_PATTERN_POS         equ LOW $

END_PAGE_TABLE("song_table")

BEGIN_PAGE_TABLE("pattern_table", false)

p00 equ LOW $
    dw empty_pattern
p02 equ LOW $
    dw drums_pattern
p04 equ LOW $
    dw unison_pattern
p06 equ LOW $
    dw event_pattern
p08 equ LOW $
    dw note_pattern0
p0a equ LOW $
    dw note_pattern1
p0c equ LOW $
    dw empty_drums_pattern
p0e equ LOW $
    dw event_pattern1

END_PAGE_TABLE("pattern_table")

; ============================================

empty_pattern:
    db 4+1

empty_drums_pattern:
    db 4+1, 4

drums_pattern:
    db 1, 96

    db 3, KICK_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, SNARE_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 5, HAT_SAMPLE_ID
    ;
    ;
    ; ----
    ;
    db 1, SNARE_SAMPLE_ID
    db 3, HAT_SAMPLE_ID
    ;
    ;
    db 2, SNARE_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, KICK_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, SNARE_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 6, HAT_SAMPLE_ID
    ;
    ;
    ; ----
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, SNARE_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, HAT_SAMPLE_ID
    ;
    ;
    db 3, KICK_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, SNARE_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, KICK_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, SNARE_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, KICK_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, SNARE_SAMPLE_ID
    ;
    ;
    db 2, HAT_SAMPLE_ID
    ;
    db 3, KICK_SAMPLE_ID
    ; ----
    ;
    db 1, KICK_SAMPLE_ID
    db 3, HAT_SAMPLE_ID
    ;
    ;
    db 2, SNARE_SAMPLE_ID
    ;
    db 1, KICK_SAMPLE_ID
    db 3, HAT_SAMPLE_ID
    ;
    ;

note_pattern0:
    db 3+1

    ; ----
    ;
    ;
    db 1, Gs3
    db 1, Note_Off
    db 1, Gs3
    db 3, Note_Off
    ;
    ;
    db 1, Gs3
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Gs3
    db 1, Note_Off
    db 1, Gs3
    db 3, Note_Off
    ;
    ;
    db 1, Gs3
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, As3
    db 1, Note_Off
    db 1, As3
    db 3, Note_Off
    ;
    ;
    db 1, As3
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, As3
    db 1, Note_Off
    db 1, As3
    db 3, Note_Off
    ;
    ;
    db 1, As3
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Cs4
    db 1, Note_Off
    db 1, Cs4
    db 3, Note_Off
    ;
    ;
    db 1, Cs4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Cs4
    db 1, Note_Off
    db 1, Cs4
    db 3, Note_Off
    ;
    ;
    db 1, Cs4
    db 2, Note_Off
    ;
    db 1, B_3
    db 4, Note_Off
    ;
    ;
    ;
    db 1, B_3
    db 3, Note_Off
    ;
    ;
    db 1, B_3
    db 2, Note_Off
    ;
    db 1, As3
    db 4, Note_Off
    ;
    ;
    ;
    db 1, As3
    db 3, Note_Off
    ;
    ;
    db 1, B_3
    db 2, Note_Off
    ;

note_pattern1:
    db 3+1

    ; ----
    ;
    ;
    db 1, Ds4
    db 1, Note_Off
    db 1, Ds4
    db 3, Note_Off
    ;
    ;
    db 1, Ds4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Ds4
    db 1, Note_Off
    db 1, Ds4
    db 3, Note_Off
    ;
    ;
    db 1, Ds4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Fs4
    db 1, Note_Off
    db 1, Fs4
    db 3, Note_Off
    ;
    ;
    db 1, Fs4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Fs4
    db 1, Note_Off
    db 1, Fs4
    db 3, Note_Off
    ;
    ;
    db 1, Fs4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Gs4
    db 1, Note_Off
    db 1, Gs4
    db 3, Note_Off
    ;
    ;
    db 1, Gs4
    db 5, Note_Off
    ;
    ; ----
    ;
    ;
    db 1, Gs4
    db 1, Note_Off
    db 1, Gs4
    db 3, Note_Off
    ;
    ;
    db 1, Gs4
    db 2, Note_Off
    ;
    db 1, Gs4
    db 4, Note_Off
    ;
    ;
    ;
    db 1, Gs4
    db 3, Note_Off
    ;
    ;
    db 1, Gs4
    db 2, Note_Off
    ;
    db 1, E_4
    db 4, Note_Off
    ;
    ;
    ;
    db 1, E_4
    db 3, Note_Off
    ;
    ;
    db 1, Fs4
    db 2, Note_Off
    ;

unison_pattern:
    db 1

    db 3, uB_2
    ;
    ;
    db 2, Cs5
    ;
    db 1, Ds5
    db 2, Note_Off
    ;
    db 1, Gs4
    db 2, uB_2
    ;
    db 1, uNote_Off
    db 3, Cs5
    ;
    ;
    db 2, uB_2
    ;
    db 1, B_4
    db 2, Note_Off
    ;
    db 1, uB_3
    db 2, uNote_Off
    ;
    db 1, uB_2
    db 2, uGs3
    ;
    db 1, Fs4
    db 2, As4
    ;
    db 1, B_4
    db 2, Note_Off
    ;
    db 1, Fs4
    db 3, uGs3
    ;
    ;
    db 3, B_4
    ;
    ;
    db 1, Cs5
    db 2, Note_Off
    ;
    db 2, uGs3
    ;
    db 1, uB_2
    db 1, uGs3
    db 2, uNote_Off
    ;
    db 2, uE_3
    ;
    db 1, uNote_Off
    db 1, B_4
    db 1, Note_Off
    db 1, uE_3
    db 2, Cs5
    ;
    db 1, Note_Off
    db 2, uE_3
    ;
    db 1, Ds5
    db 1, B_4
    db 2, Cs5
    ;
    db 2, uE_3
    ;
    db 1, uNote_Off
    db 2, B_4
    ;
    db 1, Gs4
    db 2, uE_3
    ;
    db 1, B_4
    db 2, uE_3
    ;
    db 1, uNote_Off
    db 2, Cs5
    ;
    db 1, uE_3
    db 2, D_5
    ;
    db 1, Ds5
    db 2, Note_Off
    ;
    db 1, uFs4
    db 3, Cs5
    ;
    ;
    db 2, uFs4
    ;
    db 1, Fs5
    db 2, uFs4
    ;
    db 1, B_4
    db 2, uGs3
    ;
    db 1, Ds5

event_pattern:
    db 1

    db 3, CHANNEL_A_UNISON
    ;
    ;
    db 6, CHANNEL_A_FM
    ;
    ;
    ; ----
    ;
    ;
    db 3, CHANNEL_A_UNISON
    ;
    ;
    db 3, CHANNEL_A_FM
    ;
    ;
    db 2, CHANNEL_A_UNISON
    ;
    db 3, CHANNEL_A_FM
    ; ----
    ;
    db 6, CHANNEL_A_UNISON
    ;
    ;
    ;
    ; ----
    ;
    db 7, CHANNEL_A_FM
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 3, CHANNEL_A_UNISON
    ;
    ;
    db 6, CHANNEL_A_FM
    ;
    ;
    ;
    ;
    ;
    db 9, CHANNEL_A_UNISON
    ;
    ;
    ;
    ;
    ;
    ; ----
    ;
    ;
    db 2, CHANNEL_A_FM
    ;
    db 1, CHANNEL_A_UNISON
    db 3, CHANNEL_A_FM
    ;
    ;
    db 2, CHANNEL_A_UNISON
    ;
    db 4, CHANNEL_A_FM
    ; ----
    ;
    ;
    db 3, CHANNEL_A_UNISON
    ;
    ;
    db 3, CHANNEL_A_FM
    ;
    ;
    db 2, CHANNEL_A_UNISON
    ;
    db 1, CHANNEL_A_FM
    db 3, CHANNEL_A_UNISON
    ;
    ;
    db 2, CHANNEL_A_FM
    ;
    db 1, CHANNEL_A_UNISON
    db 5, CHANNEL_A_FM
    ;
    ;
    ;
    ;
    db 1, CHANNEL_A_UNISON
    db 3, CHANNEL_A_FM
    ;
    ;
    db 2, CHANNEL_A_UNISON
    ;
    db 1, CHANNEL_A_FM
    db 2, CHANNEL_A_UNISON
    ;
    db 1, CHANNEL_A_FM
    db 2, CHANNEL_A_UNISON
    ;
    db 1, CHANNEL_A_FM

event_pattern1:
    db 1

    db 1, CHANNEL_A_INSTRUMENT
    dw bass_instrument

    db 1, CHANNEL_B_INSTRUMENT
    dw epiano_instrument

    db 1, CHANNEL_A_UNISON_INSTRUMENT
    dw bass_instrument2

    db 1, CHANNEL_C_INSTRUMENT
    dw epiano_instrument

; ============================================

END_BANK(2)

; ============================================

align 256
sawtooth_waveform_mod:
    ds 256

; ============================================

bass_waveform:
    ds 256
theremin_waveform:
    ds 256
epiano_waveform_1:
    ds 256
epiano_waveform_3:
    ds 256
epiano_waveform_5:
    ds 256
epiano_waveform_7:
    ds 256
epiano_waveform_9:
    ds 256
epiano_waveform_b:
    ds 256
epiano_waveform_d:
    ds 256
epiano_waveform_f:
    ds 256

; ============================================

BEGIN_BANK(0)

; ============================================

BEGIN_SAMPLE("kick_sample")

    import_bin "../bin/sample/wnd_kick.raw"

END_SAMPLE("kick_sample")

; ============================================

BEGIN_SAMPLE("kick2_sample")

    import_bin "../bin/sample/wnd_kick2.raw"

END_SAMPLE("kick2_sample")

; ============================================

BEGIN_SAMPLE("snare_sample")

    import_bin "../bin/sample/wnd_snare.raw"

END_SAMPLE("snare_sample")

; ============================================

BEGIN_SAMPLE("snare_light_sample")

    import_bin "../bin/sample/wnd_snare_light.raw"

END_SAMPLE("snare_light_sample")

; ============================================

BEGIN_SAMPLE("hat_sample")

    ; import_bin "../bin/sample/wnd_hat.raw"
    import_bin "../bin/sample/simmons_hat_long.raw"

END_SAMPLE("hat_sample")

; ============================================

BEGIN_SAMPLE("tom_sample")

    import_bin "../bin/sample/wnd_tom.raw"

END_SAMPLE("tom_sample")

; ============================================

BEGIN_SAMPLE("cymbal_sample")

    import_bin "../bin/sample/wnd_cymbal.raw"

END_SAMPLE("cymbal_sample")

; ============================================

END_BANK(0)
