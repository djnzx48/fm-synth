    DEVICE ZXSPECTRUM128

TR_DOS      equ $3d13               ; TR-DOS entry routine.
NXTLIN      equ $5c55               ; address of next line in program.

DZX0_SIZE   equ dzx0_standard_end-dzx0_standard
DZX0_BASE   equ $6000-DZX0_SIZE     ; location of ZX0 decompressor.
RAMTOP      equ DZX0_BASE-1

    include "../bin/entry.asm"
    include "../bin/zx0_info.asm"

decompressed_data equ $6000
decompressed_data_end equ decompressed_data+decompressed_size

compressed_data_end equ decompressed_data_end+zx0delta
compressed_data equ compressed_data_end-compressed_size

program:
    dw $0000                ; Line number (big-endian)
    dw line_0_end-line_0    ; Line length
line_0:
    db $da                  ; PAPER
    db $c3                  ; NOT
    db $a7                  ; PI
    db ':'                  ; :
    db $da                  ; INK
    db $a7                  ; PI
    db '+'                  ; +
    db $a7                  ; PI
    db ':'                  ; :
    db $e7                  ; BORDER
    db $c3                  ; NOT
    db $a7                  ; PI
    db ':'                  ; :
    db $fd                  ; CLEAR
    db '0'                  ; 0
    db $0e                  ; <number>
        db 0
        db 0
        dw RAMTOP           ; RAMTOP
        db 0
    db ':'                  ; :
        db $fb
        db ':'
    db $f9                  ; RANDOMIZE
    db $c0                  ; USR
    db $b0                  ; VAL
    db '"'                  ; "
    db $be                  ; PEEK
    db '23637'              ; 23637
    db '+'                  ; +
    db '2'                  ; 2
    db '5'                  ; 5
    db '6'                  ; 6
    db '*'                  ; *
    db $be                  ; PEEK
    db '23638'              ; 23638
    db '"'                  ; "
    ; db ':'                  ; :
    db $0d ; ???
line_0_end:

boot:

    di

    ; calculate decompressor start address in HL
    ld hl, boot_end-boot
    add hl, bc

    ; paging port
    ld bc, $7ffd
    ld a, $10

    ; page in required RAM bank
    ld ($5b5c), a
    out (c), a

    ; copy ZX0 decompressor to desired location

    ; put destination address in DE
    ld de, DZX0_BASE

    ; decompressor size in BC
    ld bc, dzx0_standard_end-dzx0_standard

    ; copy decompressor to destination address
    ldir

    ; load data block
    ld hl, compressed_data          ; address to load data
    ld de, ($5cf4)                  ; track and sector
    ld bc, zx0_sectors*$100 + 5     ; number of sectors and command number
    call TR_DOS

    di

    ; decompress loaded block
    ld hl, compressed_data
    ld de, $6000
    call DZX0_BASE

    ; start the program
    jp entry

boot_end:
    DISP DZX0_BASE
    include "dzx0_standard.asm"
dzx0_standard_end:
    ENT
program_end:

zx0_data:
    INCBIN "../bin/synth.bin.zx0"
zx0_data_end:

program_size equ program_end-program
zx0_size equ zx0_data_end-zx0_data
zx0_sectors equ high (zx0_size + $ff)

    EMPTYTRD "../build/synth.trd"
    SAVETRD "../build/synth.trd", "boot.B", program, program_size, 0
    SAVETRD "../build/synth.trd", &"boot.B", zx0_data, zx0_size
