Z_LD_B_N    equ $06
Z_LD_C_N    equ $0e
Z_LD_D_N    equ $16
Z_LD_E_N    equ $1e
Z_LD_H_N    equ $26
Z_LD_L_N    equ $2e
Z_LD_HL_N   equ $36
Z_LD_A_N    equ $3e

Z_LD_BC_NN  equ $01
Z_LD_DE_NN  equ $11
Z_LD_HL_NN  equ $21
Z_LD_SP_NN  equ $31

Z_LD_NN_HL  equ $22
Z_LD_NN_A   equ $32

Z_LD_R_HL_NN    equ $2a
Z_LD_R_A_NN     equ $3a

; with EX prefix
Z_LD_NN_BC  equ $43
Z_LD_NN_DE  equ $53
Z_LD_NN_SP  equ $73

; with EX prefix
Z_LD_R_BC_NN    equ $4b
Z_LD_R_DE_NN    equ $5b
Z_LD_R_SP_NN    equ $7b

Z_ADD_A_N   equ $c6
Z_CP_N      equ $fe

Z_JP_NN     equ $c3

Z_JR_N      equ $18

Z_PREFIX_IX equ $dd
Z_PREFIX_IY equ $fd

Z_PREFIX_SH equ $cb
Z_PREFIX_EX equ $ed
