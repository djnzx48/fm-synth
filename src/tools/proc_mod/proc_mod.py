#!/usr/bin/env python3

import sys

if __name__ == '__main__':
    count = 0x80
    while True:
        byte = sys.stdin.buffer.read(1)
        if not byte:
            break

        byte = byte[0]
        byte += count
        byte &= 0xff
        sys.stdout.buffer.write(bytes([byte]))

        count += 1
