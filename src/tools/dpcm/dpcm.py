#!/usr/bin/env python3

import sys

class Output:
    def __init__(self):
        self.first = None

    def write(self, x):
        # get x in range [0, 16)
        x = (x + 8) & 0xff

        if x >= 16:
            raise ValueError("%d out of range" % (x - 8))

        if self.first is None:
            self.first = x
        else:
            # output packed nibbles to stdout
            byte = bytes([(self.first << 4) | x])
            sys.stdout.buffer.write(byte)
            self.first = None

if __name__ == '__main__':
    data = bytes()

    while True:
        byte = sys.stdin.buffer.read(1)
        if not byte:
            break

        data += byte

    out = Output()

    out.write(data[0])
    out.write(data[1] - data[0])

    for i in range(2, len(data)):
        out.write(data[i-2] + data[i] - 2*data[i-1])
