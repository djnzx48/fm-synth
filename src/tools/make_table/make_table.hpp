#pragma once

#include <array>
#include <memory>

using Chip_vol = std::array<int, 3>;

// specify low and high or set both to zero if don't care
std::unique_ptr<std::array<Chip_vol, 256>> best_table(const std::array<int, 16>& levels, int low, int high, bool use_weights);

std::unique_ptr<std::array<int, 256>> best_table_4bit(const std::array<int, 16>& levels, int low, int high);
