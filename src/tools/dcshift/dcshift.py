#!/usr/bin/env python3

import sys
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('amount', type=int, help='amount to shift (modulo 0x100)')

    args = parser.parse_args()

    amount = args.amount & 0xff

    while True:
        byte = sys.stdin.buffer.read(1)
        if not byte:
            break

        byte = byte[0]
        byte = max(0x60, byte)
        byte = min(byte, 0x9f)

        byte += amount
        byte &= 0xff
        sys.stdout.buffer.write(bytes([byte]))
