SAMPLEGEN_FM            equ 100
SAMPLEGEN_DUAL_FM       equ 101
SAMPLEGEN_OSC_SYNC      equ 102
SAMPLEGEN_BASS_FM       equ 103
SAMPLEGEN_BASS_UNISON   equ 104

; Outputs a single sample, mixing from both output buffers.
;
; Channel write order alternates between ABC and CBA for performance
; reasons, as the selected AY register 'sticks' between writes and can
; be rewritten to multiple times.
;
; A single use of SampleOut takes 134 T-states (not including
; I/O contention).
;
; More crucially, a single use of SampleOut always takes 25 bytes,
; neither more nor less.
;
; TODO: we can skip the 'inc l' on the final iteration.
sample_out_counter = 0

SampleOut macro(count)

    if (::sample_out_counter >= SAMPLE_OUT_COUNT)
        zeuserror "SampleOut used too many times!"
    endif

    SampleOut_oldpos = $
    SampleOut_oldbank = current_bank

    noflow

    END_BANK((SampleOut_oldbank))

org sample_out_patch_table + 3*sample_out_counter

    dw SampleOut_oldpos
    db count

    ::sample_out_counter = ::sample_out_counter + 1

    BEGIN_BANK((SampleOut_oldbank))

    loop SAMPLE_OUT_ROUTINE_SIZE
        nop
    lend

mend
