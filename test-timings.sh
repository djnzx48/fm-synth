#!/usr/bin/env bash

timeout 10 stdbuf -oL fuse --speed 100000 build/synth.tap >synth.log

cat synth.log | grep ^SD | cut -d" " -f2 | awk '/^-/ { $1 += 70908; } { print; }' | sort | uniq -c | sort -n

echo

cat synth.log | grep ^SD | cut -d" " -f2,4- | awk '/^-/ { $1 += 70908; } { print; }' | grep -v 331 | grep -v '79[79]' | sort | uniq -c | sort -n

mkdir -p build
# zstd -c synth.log >build/synth.log.zst
